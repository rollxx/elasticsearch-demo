package ch.tamedia.elasticsearch.hackathon;

import java.util.UUID;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "all-index")
@Data
public abstract class AbstractDocument {
    @Id
    private UUID id = UUID.randomUUID();
    private String text;
}
