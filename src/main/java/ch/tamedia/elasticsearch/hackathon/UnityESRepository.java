package ch.tamedia.elasticsearch.hackathon;

import java.util.Locale;
import java.util.UUID;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UnityESRepository<D extends AbstractDocument> extends ElasticsearchRepository<D, UUID> {

//    Locale getLocale();
    Iterable<D> findAllByText(String text);
}
