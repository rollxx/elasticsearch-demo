//package ch.tamedia.elasticsearch.hackathon;
//
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Service;
//
//@RequiredArgsConstructor
//@Service
//public class MappingService {
//
//    private final UnityESRepository<GermanDocument> germanDocumentUnityESRepository;
//    private final UnityESRepository<FrenchDocument> frenchDocumentUnityESRepository;
//
//    Map<Locale, UnityESRepository<? extends AbstractDocument>> map = new HashMap<>();
//
//    public UnityESRepository<? extends AbstractDocument> getRepository(Locale locale) {
//        return map.get(locale);
//    }
//
//    @PostConstruct
//    public void init() {
//        map.put(germanDocumentUnityESRepository.getLocale(), germanDocumentUnityESRepository);
//        map.put(frenchDocumentUnityESRepository.getLocale(), frenchDocumentUnityESRepository);
//    }
//}
