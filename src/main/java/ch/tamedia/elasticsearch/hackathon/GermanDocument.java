package ch.tamedia.elasticsearch.hackathon;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Setting;

@EqualsAndHashCode(callSuper = true)
@Data
@Document(indexName = "german-index")
@Setting(settingPath = "settings/german.json")
public class GermanDocument extends AbstractDocument {
}
