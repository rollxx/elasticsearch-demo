package ch.tamedia.elasticsearch.hackathon;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class Controller {

    private final GermanRepository repository;

    @GetMapping
    public Iterable<? extends AbstractDocument> get(@RequestParam("q") String q) {
        return repository.findAllByText(q);
    }

    @PostMapping
    public GermanDocument create(@RequestBody GermanDocument document) {
        return repository.save(document);
    }
}
