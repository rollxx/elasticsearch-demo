package ch.tamedia.elasticsearch.hackathon;

import java.util.Locale;
import java.util.UUID;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface FrenchRepository extends UnityESRepository<FrenchDocument> {

//    @Override
//    default Locale getLocale() {
//        return new Locale("fr", "CH");
//    }
}
